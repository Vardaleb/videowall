import QtQuick 2.9
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Window 2.0
import QtMultimedia 5.15
import Videowall 1.0


ApplicationWindow {
    id: mainWindow
    visible: true
    width: 1920
    height: 1080
    title: qsTr("Video-Wall")
    visibility: Window.FullScreen

    VideoController
    {
        id: videoController
    }
    
    Item
    {
        focus: true
        Keys.onPressed: 
        {
            if( event.key === Qt.Key_Escape )
                Qt.callLater(Qt.quit)
            else
            {
                if( mainWindow.visibility == Window.FullScreen )
                    mainWindow.visibility = Window.Windowed;
                else
                    mainWindow.visibility = Window.FullScreen;
            }
        }
    }
    
    GridLayout {
        id: wall
        anchors.fill: parent
        rowSpacing: 0
        columnSpacing: 0

        Component.onCompleted:
        {
            wall.columns = videoController.grid
            wall.rows = videoController.grid
            for( var r=0; r<wall.rows; ++r )
            {
                for( var c=0; c<wall.columns; c++)
                    createCell();
            }
        }

        function createCell()
        {
            var component = Qt.createComponent("VideoCell.qml", wall);
            var cell = component.createObject( wall,
              {
                  "Layout.fillHeight" : true,
                  "Layout.fillWidth" : true
              } );
        }
    }
}

import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    Layout.alignment: Qt.AlignCenter

//    Text {
//        anchors.centerIn: parent
//        text: video.source
//    }

    RowLayout {
        anchors.centerIn: parent
        spacing: 2

        RoundButton {
            // text: video.muted ? "Unmute" : "Mute"
            icon.name: video.muted ? "audio-volume-high" : "audio-volume-muted"
            focusPolicy: Qt.NoFocus
            onClicked: video.muted = !video.muted

            ToolTip.visible: hovered
            ToolTip.text: video.muted ? "Unmute" : "Mute"
        }

        RoundButton {
            icon.name: "help-about"
            focusPolicy: Qt.NoFocus
            ToolTip.visible: hovered
            ToolTip.text: video.source
        }

        RoundButton {
            // text: "Next"
            icon.name: "media-seek-forward"
            focusPolicy: Qt.NoFocus
            onClicked: load()
            ToolTip.visible: hovered
            ToolTip.text: "Next"
        }
    }
}

/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  Andreas Blochberger <andreas@blochberger.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "videocontroller.h"
#include <QCommandLineParser>
#include <QDir>
#include <QRandomGenerator>
#include <QCommandLineOption>
#include "argumentparser.h"

VideoController::VideoController()
{
    for( int i=0; i<2; ++i )
        files[i] = new QStringList();
    current = files[0];
    buffer = files[1];

    QCommandLineParser parser;
    QCommandLineOption gridOption(
                QStringList() << "g" << "grid",
                "Number of rows and columns",
                "grid",
                "2");

    parser.addOption(gridOption);
    parser.process(QCoreApplication::arguments());
    grid = parser.value(gridOption).toInt();

    ArgumentParser argumentParser( parser.positionalArguments() );
    for( QString& s : *argumentParser.getUrls() )
    {
        qDebug() << "Add file " << s << " to available files" << Qt::endl;

        current->append(s);
    }
}

VideoController::~VideoController()
{
    for( int i=0; i<2; ++i )
        delete files[i];
}


QString VideoController::getVideo()
{
    if( current->size() == 0 )
    {
        qDebug() << "switch buffer..." << Qt::endl;
        buffer = current;
        current = files[++currentIndex%2];
    }

    if( current->size() == 0 )
    {
        qDebug() << "Nothing in current" << Qt::endl;
        return "";
    }

    int index = QRandomGenerator::global()->bounded(current->size());
    QString result = current->at(index);

    qDebug() << "Move index " << index << "(" << result << ") from current to buffer" << Qt::endl;
    current->removeAt(index);
    buffer->append(result);

    qDebug() << "Size of current: " << current->size() << ", Size of buffer: " << buffer->size() << Qt::endl;
    return result;
}

int VideoController::getGrid()
{
    if( grid <= 0 )
        return 2;
    return grid;
}

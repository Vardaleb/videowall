#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include <QStringList>
#include <QUrl>
#include <QDebug>

class ArgumentParser
{
    QStringList* urls;

    void loadFromStringList(QStringList arguments );
    void loadFromStdin();
    void loadFiles( QUrl directoryName );

public:

    ArgumentParser( QStringList arguments );
    ~ArgumentParser()
    {
        delete urls;
    }

    QStringList* getUrls()
    {
        qDebug() << "URLs: " << urls->size() << Qt::endl;
        for( QString s : *urls )
            qDebug() << " URL: " << s << Qt::endl;

        return urls;
    };
};

#endif // ARGUMENTPARSER_H

/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  Andreas Blochberger <andreas@blochberger.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VIDEOCONTROLLER_H
#define VIDEOCONTROLLER_H

#include <QObject>
#include <qqml.h>

class VideoController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString video READ getVideo)
    Q_PROPERTY(int grid READ getGrid)
    QML_ELEMENT

private:
    QStringList* files[2];
    int currentIndex = 0;
    QStringList* current;
    QStringList* buffer;
    int grid = 2;

public:
    /**
     * Default constructor
     */
    VideoController();

    /**
     * Destructor
     */
    virtual ~VideoController();
    
private:

    QString getVideo();
    int getGrid();
    void loadFiles( QString directory );
};

#endif // VIDEOCONTROLLER_H

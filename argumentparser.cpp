#include <QDir>
#include "argumentparser.h"
#include <QTextStream>

ArgumentParser::ArgumentParser(QStringList arguments )
{
    urls = new QStringList();
    if( arguments.size() > 0 )
    {
        qDebug() << "Load from command line arguments" << arguments << Qt::endl;

        loadFromStringList( arguments );
    }
    else
        loadFromStdin();
}

void ArgumentParser::loadFromStdin()
{
    qDebug() << "Load from stdin" << Qt::endl;

    QStringList arguments;
    QTextStream qtin(stdin);
    QString word;
    while( !qtin.atEnd())
    {
        qDebug() << word << Qt::endl;
        qtin >> word;
        if( !word.isEmpty())
            arguments.append(word);
    }

    loadFromStringList(arguments);
}

void ArgumentParser::loadFromStringList(QStringList arguments )
{
    qDebug() << "Stringlist " << arguments << Qt::endl;

    for( const auto& argument : qAsConst(arguments) )
    {
        QUrl* url;
        if( argument.startsWith("/"))
            url = new QUrl( QUrl::fromLocalFile(argument) );
        else
            url = new QUrl( argument );

        if( url->isLocalFile() )
        {
            QFileInfo file(url->toLocalFile());
            if( file.isDir())
                loadFiles( *url );
            else
                urls->append( url->toDisplayString());
        }
        else
            urls->append( url->toDisplayString());
        delete url;
    }
}

void ArgumentParser::loadFiles( QUrl directoryUrl )
{
    QDir directory( directoryUrl.toLocalFile() );
    QStringList files = directory.entryList(QStringList() << "*.mp4",QDir::Files);
    for( const auto& file : qAsConst(files) )
    {
        QUrl url;
        url.setScheme(directoryUrl.scheme());
        url.setPath(directoryUrl.path() + "/" + file);
        urls->append( url.toDisplayString() );
    }
}

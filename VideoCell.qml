import QtQuick 2.9
import QtMultimedia 5.15
import QtQuick.Controls 2.15

Video {
    id: video
    muted: true

    Popup {
        id: popup
        parent: video

        x: Math.round((parent.width - width) / 2)
        y: parent.height - Math.round(parent.height * 0.3)
        height: Math.round(parent.height * 0.15)

        enter: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        background: Rectangle {
            color: "#77FFFFFF"
            border.color: "transparent"
            radius: 20
        }

        contentItem: VideoControls {
        }

    }
    
    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton
        
        onClicked: 
        {
            if( !popup.visible )
            {
                if( mouse.button& Qt.LeftButton )
                    muted = !muted;
                else if( mouse.button& Qt.RightButton  )
                    showMenu();
                else if( mouse.button& Qt.MiddleButton )
                {
                    muted = true;
                    load();
                }
            }
        }
    }

    Component.onCompleted:
    {
        load();
    }

    onStopped:
    {
        load();
    }

    function load()
    {
        source = videoController.video;
        play();
    }

    function showMenu()
    {
        popup.open();
    }
}


